# Ejemplo API .NET

Ejemplo de una API REST creada con .NET y Entity Framework

Creada con SQL Server 2014, .NET Framework 4.7.2, Entity Framework 6 y Visual Studio 2019.

## Base de datos

Se utilizará la base de datos de una librería. Para poblar la base de datos ejecute en SQL Server Management Studio el script ubicado en `BD/database.sql`.

## Instalación

1. Limpie y compile la solución antes de ejecutar el proyecto `WebApi`.
2. Revise que la conexión a la base de datos esté funcionando correctamente.
1. Ejecute el proyecto presionando la tecla `F5`.

> Puede revisar los servicios de prueba con Postman con el archivo `Postman/Ejemplo API .NET.postman_collection.json`.