﻿using Datos.Modelo;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    /// <summary>
    /// Servicios REST para la tabla Libro
    /// </summary>
    public class LibroController : ApiController
    {
        LibreriaConnectionEntities BD = new LibreriaConnectionEntities();

        /// <summary>
        /// Obtener lista de todos los libros de la base de datos
        /// </summary>
        /// <returns>Lista de todos los libros</returns>
        [HttpGet]
        public IEnumerable<Libro> Get()
        {
            var listado = BD.Libro.ToList();
            return listado;
        }

        /// <summary>
        /// Obtener un libro
        /// </summary>
        /// <param name="id">Id del libro</param>
        /// <returns>Datos del libro especificado</returns>
        [HttpGet]
        public Libro Get(int id)
        {
            var libro = BD.Libro.FirstOrDefault(x => x.IdLibro == id);
            return libro;
        }

        /// <summary>
        /// Agregar un libro a la base de datos
        /// </summary>
        /// <param name="libro">El objeto "Libro" con sus atributos</param>
        [HttpPost]
        public IHttpActionResult Post([FromBody]Libro libro)
        {
            if (ModelState.IsValid)
            {
                BD.Libro.Add(libro);
                BD.SaveChanges();
                return Ok(libro);
            } 
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Modificar un libro existente en la base de datos
        /// </summary>
        /// <param name="id">El identificador del libro</param>
        /// <param name="libro">El objeto "Libro" con los atributos a modificar</param>
        [HttpPut]
        public IHttpActionResult Put(int id, [FromBody]Libro libro)
        {
            if (ModelState.IsValid)
            {
                var libroExiste = BD.Libro.Count(c => c.IdLibro == id) > 0;

                if (libroExiste)
                {
                    BD.Entry(libro).State = EntityState.Modified;
                    BD.SaveChanges();

                    return Ok(libro);
                } else
                {
                    return NotFound();
                }
            }
            else
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Eliminar in libro de la base de datos
        /// </summary>
        /// <param name="id">El identificador del libro</param>
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var libro = BD.Libro.Find(id);

            if (libro != null)
            {
                BD.Libro.Remove(libro);
                BD.SaveChanges();
                return Ok(libro);
            } 
            else
            {
                return NotFound();
            }
        }

    }
}
